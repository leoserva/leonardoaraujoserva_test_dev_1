package backend

import grails.gorm.transactions.Transactional
import groovy.time.TimeCategory
import groovy.time.TimeDuration

@Transactional
class StockService {

    def getStocks(String company, Integer numbersOfHoursUntilNow) {

        Date start = new Date()

        def askedCompany = Company.findByName(company)

        def lastDate = new Date()

        use( TimeCategory ) {
            lastDate = lastDate - numbersOfHoursUntilNow.intValue().hours
        }

        def stockQuotes = Stock.findAllByCompany(askedCompany)

        Closure queryPriceDate = { it.priceDate >= lastDate }

        stockQuotes = stockQuotes.findAll(queryPriceDate)

        stockQuotes.each { println it }

        Date stop = new Date()

        TimeDuration timeDuration = TimeCategory.minus( stop, start )

        println "Total time: " + timeDuration.getMillis() + " milliseconds"

        println "Number of quotes: " + stockQuotes.size()

        stockQuotes
    }
}
