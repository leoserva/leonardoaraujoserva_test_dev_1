package backend

import groovy.transform.ToString

@ToString(includes = 'company, price, priceDate', includePackage = false, includeNames = true)
class Stock {

    Company company
    Double price
    Date priceDate

    static constraints = {
    }

}
