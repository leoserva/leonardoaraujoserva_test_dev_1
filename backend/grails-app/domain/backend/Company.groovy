package backend

import groovy.transform.ToString

@ToString(includes = 'name, segment', includePackage = false, includeNames = true)
class Company {

    String name
    String segment

    static constraints = {
    }

}
