package backend

import groovy.time.TimeCategory

class BootStrap {

    def init = { servletContext ->

        def c1 = new Company(name: "FORD", segment: "vehicles").save()
        def c2 = new Company(name: "WALMART", segment: "supermarket").save()
        def c3 = new Company(name: "KRAFT", segment: "foods").save()

        def totalDays = 30
        def minutesOfDay = 60 * 8
        Date fromDate = new Date()

        fromDate.clearTime()

        Date toDate = new Date()

        use( TimeCategory ) {
            fromDate = fromDate + 10.hours
            fromDate = fromDate - totalDays.days

            fromDate.upto(toDate) {
                for (i in 0..minutesOfDay.intValue()) {
                    def newDate = it + i.minutes
                    if (newDate <= toDate) {
                        new Stock(company: c1, price: 10, priceDate: newDate).save()
                        new Stock(company: c2, price: 20, priceDate: newDate).save()
                        new Stock(company: c3, price: 30, priceDate: newDate).save()
                    }
                }
            }
        }
    }

    def destroy = {
    }

}
